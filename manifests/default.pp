Exec { path => [ '/usr/bin:/usr/sbin:/bin:/usr/local/bin:/usr/local/sbin:/sbin' ] }

include system-update
include nginx
include samba
include postfix
include dovecot
include php5
include mysql
include roundcube
include squid
include vsftpd
include ssh
include iptables

# Instala pacotes adicionais
package {['nano', 'lynx']:
  require => Class["system-update"],
  ensure  => 'installed',
}

# Copia os arquivos de README para raiz /var/www/
exec {'copia_readme':
    command => 'sudo cp /vagrant/README.html /usr/share/nginx/www/index.html',
    onlyif  => ['ls -l /vagrant/README.html'],
    require => Package['nginx']
}
