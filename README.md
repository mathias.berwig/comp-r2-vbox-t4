# Redes II - Trabalho Final - Virtualização de múltiplos serviços

Este trabalho foi desenvolvido durante a disciplina de Redes II do curso de Ciência da Computação na UNIJUÍ e tem como objetivo demonstrar o procedimento para criar um ambiente virtualizado contendo os servidores/tecnologias: Nginx e FTP (reutilizando a implementação do trabalho anterior), Squid, PostFix, IP Tables, SSH e Samba. Além disso, é necessário implementar uma política de segurança personalizada.

## Workspace

Para configurar o ambiente de trabalho, é necessário instalar o Vagrant, Puppet e VirtualBox na máquina. É possível fazer isso em um sistema operacional Linux baseado em Debian utilizando o seguinte comando:

`sudo apt install vagrant puppet virtualbox`

## Execução

Após instalar as dependências necessárias, basta navegar até a pasta deste README  iniciar o Vagrant:

`vagrant up`

Quando o comando for concluído, o endereço HTTP [`http:\\192.168.33.16`](http:\\192.168.33.16) estará disponível com um arquivo README; e o endereço FTP [`ftp://192.168.33.16`](ftp://192.168.33.16) estará acessível com as credenciais `vagrant` `vagrant`.

## Descrição do trabalho

### VM precise64 com aplicação de uma Política de Segurança

Disponibilizar um ambiente Linux (Box Vagrant/Puppet) com uma Política de Segurança aplicada.

A presente atividade consiste no fechamento das atividades anteriores sendo necessário que todos os serviços anteriormente estudados e implementados estejam presente na aplicação de uma Politica de Segurança definido pelo aluno.

Os serviços em questão são email, ftp, samba e Squid. Podem ainda ser adicionados outros serviços para contemplar a implantação da politica definida.  

Além dos serviços já configurados e estudados nas atividades anteriores, nesta atividade o aluno deve adicionar o serviço de firewall, sugere-se a utilização do IPTABLES ou mesmo IPFW. Necessariamente, o aluno deve, primeiramente, estudar e definir uma politica de segurança para seu ambiente, no caso o Box a ser desenvolvido. Após esta definição a politica deve ser apresentada claramente (entregue um arquivo em PDF onde consta a escrita desta política) e implementada a partir do uso de todos os serviços já trabalhados nas atividades anteriores.

A dimensão, abrangência e principalmente a implantação e funcionamento desta politica no Box desenvolvido será avaliada.

Juntamente com o arquivo em PDF que consta a política de segurança descrita deve existir uma seção ou mesmo um anexo, conforme o caso, onde esteja explicado de forma clara e objetiva como cada item da política foi implementado. Sugere-se uma estrutura da seguinte forma:

O item 1 da política de segurança foi implementado usando o serviço Squid a partir da aplicação de filtros que estão definidos no arquivo proibidos.txt no diretório /etc/squid/sites.

O que entregar no dia *10 de Dez/2018* de forma individual:

- Um box configurado com a aplicação da politica de segurança implantada. Executando sem erros (vagrantfile)
- Um relatório contento a política de segurança e um descritivo completo de como a mesma foi implementada.

### Sugestão de Leitura

- https://cartilha.cert.br/
- https://www.cert.br/
- https://www.cert.br/docs/seg-adm-redes/seg-adm-redes.html
- https://www.cert.br/docs/seg-adm-redes/seg-adm-redes.pdf

## Política Escolhida

### Gerenciador de e-mail

- não enviar anexos com extensões `bat, com, exe, dll, vbs`;
- não enviar anexos maiores do que 2MB;
- não enviar para mais de 5 destinatários ao mesmo tempo.

### FTP

- limitar a velocidade de upload a 5 Mb/s;
- não permitir acesso sem usuário e senha;
- não permitir upload de arquivos com extensões `.mp3 e .mov`;
- não permitir mais de 2 clientes simultâneos de IPs diferentes;
- não permitir acesso ftp, apenas sftp.

### SSH

- não permitir mais de `2` usuários simultâneos;
- não permitir mais de `2` tentativas na senha.

### SAMBA

- não permite mais do que `10` shares por usuário;
- não permite o acesso de forma publica as pastas;

### Squid

- não permitir acesso às redes sociais `facebook, twitter, reddit`;
- não permitir acesso aos sites `pornhub` e `xvideos`;
- não permitir acesso a sites relacionados a `comunismo` ou `ditadura`;
- os logs devem ser salvos no FTP (`/home/vagrant/logs)`.