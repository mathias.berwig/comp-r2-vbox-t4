class squid {

  package { 'squid':
    ensure  => present,
    require => Class['system-update'],
  }

  service { 'squid3':
    ensure  => 'running',
    require => Package['squid'],
  }

  file {'/usr/share/nginx/www/squid/':
    ensure  => directory,
    require => Class['nginx'],
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
  }

  file {'/usr/share/nginx/www/squid/index.html':
    ensure  => file,
    require => Package['nginx'],
    owner   => 'root',
    group   => 'root',
    mode    => '0755',
    source  => 'puppet:///modules/squid/index.html',
  }

  file {'/etc/squid3/formato_arquivo.txt':
      ensure  => file,
      require => Package['squid'],
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      source  => 'puppet:///modules/squid/formato_arquivo.txt',
  }

  file {'/etc/squid3/ips_liberados.txt':
      ensure  => file,
      require => Package['squid'],
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      source  => 'puppet:///modules/squid/ips_liberados.txt',
  }

  file {'/etc/squid3/palavras_bloqueadas.txt':
      ensure  => file,
      require => Package['squid'],
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      source  => 'puppet:///modules/squid/palavras_bloqueadas.txt',
  }

  file {'/etc/squid3/redes_sociais.txt':
      ensure  => file,
      require => Package['squid'],
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      source  => 'puppet:///modules/squid/redes_sociais.txt',
  }

  file {'/etc/squid3/squid.conf':
      ensure  => file,
      require => Package['squid'],
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      source  => 'puppet:///modules/squid/squid.conf',
  }

  exec {'restartSquid':
      command => 'sudo service squid3 restart',
      require => File['/etc/squid3/squid.conf']
  }

  file {'/etc/squid3/sites_bloqueados.txt':
      ensure  => file,
      require => Package['squid'],
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      source  => 'puppet:///modules/squid/sites_bloqueados.txt',
  }
}
