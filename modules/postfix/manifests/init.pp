class postfix {

  #Realiza a instalação do pacote postfix, garantindo que o system-update foi realizado
  package { 'postfix':
    ensure  => present, # para garantir que o pacote esteja disponivel e atualizado
    require => Class['system-update'],
  }

  #Realiza a instalação do pacote mailutils
  package { 'mailutils':
    ensure  => present,
    require => Class['system-update'],
  }

  #Inicializa o serviço postfix, após garantir que o arquivo de configuração (main.cf) foi criado com sucesso
  service { 'postfix':
    ensure     => running,
    enable     => true,
    hasrestart => true,
    hasstatus  => true,
    require    => File['/etc/postfix/main.cf', '/etc/postfix/mime_header_checks'],
  }

  #Copia os arquivos de configuração para dentro da pasta do postfix
  file { '/etc/postfix/main.cf':
    source  => 'puppet:///modules/postfix/main.cf',
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    require => Package['postfix'],
    notify  => Service['postfix'],
  }
  file { '/etc/postfix/mime_header_checks':
    source  => 'puppet:///modules/postfix/mime_header_checks',
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    require => Package['postfix'],
    notify  => Service['postfix'],
  }

  file {'/etc/nginx/sites-available/default':
      ensure  => file,
      require => Class['nginx'],
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      source  => 'puppet:///modules/postfix/default',
  }

  file {'/usr/share/nginx/www/postfix/index.html':
      ensure  => file,
      require => Class['nginx'],
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      source  => 'puppet:///modules/postfix/tutorial/index.html',
  }

  file {'/usr/share/nginx/www/postfix/':
    ensure  => directory,
    require => Class['nginx'],
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
  }

  file {'/usr/share/nginx/www/postfix/assets/':
    ensure  => directory,
    require => Class['nginx'],
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
  }

  file {'/usr/share/nginx/www/postfix/assets/css':
    ensure  => directory,
    require => File['/usr/share/nginx/www/postfix/assets/'],
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
  }

  file {'/usr/share/nginx/www/postfix/assets/img':
    ensure  => directory,
    require => File['/usr/share/nginx/www/postfix/assets/'],
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
  }

  file {'/usr/share/nginx/www/postfix/assets/css/bootstrap.min.css':
    ensure  => directory,
    require => File['/usr/share/nginx/www/postfix/assets/'],
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => 'puppet:///modules/postfix/tutorial/assets/css/bootstrap.min.css',
  }

  file {'/usr/share/nginx/www/postfix/assets/css/estilo.css':
    ensure  => directory,
    require => File['/usr/share/nginx/www/postfix/assets/'],
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => 'puppet:///modules/postfix/tutorial/assets/css/estilo.css',
  }

  file {'/usr/share/nginx/www/postfix/assets/img/tree.jpg':
    ensure  => directory,
    require => File['/usr/share/nginx/www/postfix/assets/'],
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => 'puppet:///modules/postfix/tutorial/assets/img/tree.jpg',
  }

  file {'/usr/share/nginx/www/postfix/assets/Trabalho_2.zip':
    ensure  => directory,
    require => File['/usr/share/nginx/www/postfix/assets/'],
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => 'puppet:///modules/postfix/Trabalho_2.zip',
  }

  file {'/usr/share/nginx/www/postfix/teste.php':
    ensure  => directory,
    require => File['/usr/share/nginx/www/postfix/assets/'],
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => 'puppet:///modules/postfix/tutorial/teste.php',
  }
}
