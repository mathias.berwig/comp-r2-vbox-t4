class mysql {
  #Realiza o download e instalação do pacote mysql-server
    package { 'mysql-server':
    ensure  => installed, # para garantir que o pacote esteja disponivel e atualizado
    require => Class['system-update'],
  }

  #Garante que o serviço do mysql estará rodando após a instalação do pacote
  service { 'mysql':
    ensure  => 'running',
    require => Package['mysql-server'],
  }
}
