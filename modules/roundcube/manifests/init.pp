class roundcube {

  #Instala os pacotes do Roundcube, garantindo que o Nginx já esteja instalado, e que o serviço do MySQL esteja rodando.
  package { ['roundcube', 'roundcube-plugins', 'roundcube-plugins-extra']:
    ensure  => installed,
    require => [Class['nginx'], Service['mysql']],
    notify  => Service['php5-fpm']
  }

  #Cria um link da pasta do Roundcube para a pasta do servidor web.
  exec { 'config-nginx':
    command => 'ln -fs /var/lib/roundcube /usr/share/nginx/www/',
    require => Package['roundcube']
  }

  #Envia o arquivo de configuração do banco de dados do Roundcube a partir de um template pré-configurado
  file { '/etc/roundcube/debian-db.php':
    ensure  => file,
    require => Package['roundcube'],
    content => template('roundcube/debian-db.php.erb')
  }

  #Envia o arquivo de configuração do Roundcube a partir de uma template pré-configurado
  file { '/etc/roundcube/main.inc.php':
    ensure  => file,
    require => Package['roundcube'],
    content => template('roundcube/main.inc.php.erb')
  }

  exec {'restartRoundCube':
    command => 'sudo service nginx restart',
    require => File['/etc/roundcube/main.inc.php'],
  }
}
