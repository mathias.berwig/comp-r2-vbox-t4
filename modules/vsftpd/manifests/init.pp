class vsftpd {
  # Instala o pacote vsftpd após garantir que a lista de pacotes está atualizada
  package { [ 'vsftpd' ]:
    ensure  => 'installed',
    require => Class['system-update'],
  }

  # Cria o diretório para hospedar a documentação
  file {'/usr/share/nginx/www/vsftpd/':
    ensure  => directory,
    require => Class['nginx'],
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
  }

  # Copia documentação
  file {'/usr/share/nginx/www/vsftpd/index.html':
      ensure  => file,
      require => Class['nginx'],
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
      source  => 'puppet:///modules/vsftpd/index.html',
  }

  # Copia os arquivos de configuração do servidor FTP & Reinicia-o
  exec {'setup_vsftpd':
      command => 'sudo cp -rf /vagrant/vsftpd.conf /etc/vsftpd.conf; sudo restart vsftpd',
      onlyif  => ['ls -l /etc/vsftpd.conf'],
      require => Package['vsftpd']
  }
  exec {'limit_band':
      command => 'sudo tc qdisc add dev eth1 root tbf rate 1mbit burst 1024kbit latency 20ms',
      onlyif  => ['ls -l /etc/vsftpd.conf'],
      require => Package['vsftpd']
  }
}
